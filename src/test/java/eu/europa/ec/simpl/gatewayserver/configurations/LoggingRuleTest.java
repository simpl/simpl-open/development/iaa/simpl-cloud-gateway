package eu.europa.ec.simpl.gatewayserver.configurations;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.mock.http.server.reactive.MockServerHttpRequest;
import org.springframework.mock.web.server.MockServerWebExchange;
import reactor.test.StepVerifier;

class LoggingRuleTest {

    @Test
    void testMatches_MatchesPathAndMethod() {
        var rule = new LoggingRule(HttpMethod.GET, "/test", null, null, null);
        var exchange =
                MockServerWebExchange.from(MockServerHttpRequest.get("/test").build());

        StepVerifier.create(rule.matches(exchange)).expectNext(true).verifyComplete();
    }

    @Test
    void testMatches_MatchesHeaders() {
        var headerRule = new LoggingRule.MultiMapRule("X-Test", null, List.of("value"));
        var rule = new LoggingRule(HttpMethod.GET, "/test", null, List.of(headerRule), null);
        var exchange = MockServerWebExchange.from(
                MockServerHttpRequest.get("/test").header("X-Test", "value").build());

        StepVerifier.create(rule.matches(exchange)).expectNext(true).verifyComplete();
    }

    @Test
    void testMatches_DoesNotMatchHeaders() {
        var headerRule = new LoggingRule.MultiMapRule("X-Test", null, List.of("value"));
        var rule = new LoggingRule(HttpMethod.GET, "/test", null, List.of(headerRule), null);
        var exchange = MockServerWebExchange.from(
                MockServerHttpRequest.get("/test").header("X-Test", "wrong").build());

        StepVerifier.create(rule.matches(exchange)).expectNext(false).verifyComplete();
    }

    @Test
    void testMatches_DoesNotMatchPath() {
        var rule = new LoggingRule(HttpMethod.GET, "/test", null, null, null);
        var exchange =
                MockServerWebExchange.from(MockServerHttpRequest.get("/wrong").build());

        StepVerifier.create(rule.matches(exchange)).expectNext(false).verifyComplete();
    }

    @Test
    void testMatches_MatchesQueryParams() {
        var queryRule = new LoggingRule.MultiMapRule("param", null, List.of("value"));
        var rule = new LoggingRule(HttpMethod.GET, "/test", List.of(queryRule), null, null);
        var exchange = MockServerWebExchange.from(
                MockServerHttpRequest.get("/test").queryParam("param", "value").build());

        StepVerifier.create(rule.matches(exchange)).expectNext(true).verifyComplete();
    }
}
