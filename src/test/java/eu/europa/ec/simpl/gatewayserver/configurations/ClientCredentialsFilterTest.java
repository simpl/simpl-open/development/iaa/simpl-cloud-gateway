package eu.europa.ec.simpl.gatewayserver.configurations;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@ExtendWith(MockitoExtension.class)
public class ClientCredentialsFilterTest {

    public static final String ROLE_CLAIM = "client-roles";

    @Mock
    private ReactiveJwtDecoder jwtDecoder;

    @Mock
    private GatewayFilterChain chain;

    private ClientCredentialsFilter filter;

    private ServerWebExchange exchange;

    @BeforeEach
    void setUp() {
        filter = new ClientCredentialsFilter(jwtDecoder);
        filter.setRoleClaim(ROLE_CLAIM);
        exchange = mockExchangeWithAuthHeader();
    }

    @Test
    void testInvalidClientId() {

        Jwt jwt = createJwt("wrong-client", Arrays.asList("admin", "user"));

        when(jwtDecoder.decode(anyString())).thenReturn(Mono.just(jwt));

        GatewayFilter gatewayFilter = filter.apply(Map.of("test-client", "admin,user"));
        StepVerifier.create(gatewayFilter.filter(exchange, chain)).verifyError();

        verify(exchange.getResponse(), times(1)).setStatusCode(HttpStatus.UNAUTHORIZED);
    }

    @Test
    void testMissingRequiredRole() {

        Jwt jwt = createJwt("test-client", List.of("admin,guest"));
        when(jwtDecoder.decode(anyString())).thenReturn(Mono.just(jwt));

        GatewayFilter gatewayFilter = filter.apply(Map.of("test-client", "admin,user"));
        StepVerifier.create(gatewayFilter.filter(exchange, chain)).verifyError();

        verify(exchange.getResponse(), times(1)).setStatusCode(HttpStatus.UNAUTHORIZED);
    }

    @Test
    void testNoRoleFromConfiguration() {

        Jwt jwt = createJwt("test-client", Arrays.asList("admin", "user"));

        when(jwtDecoder.decode(anyString())).thenReturn(Mono.just(jwt));
        when(chain.filter(exchange)).thenReturn(Mono.empty());

        GatewayFilter gatewayFilter = filter.apply(Map.of("test-client", ""));
        StepVerifier.create(gatewayFilter.filter(exchange, chain)).verifyComplete();

        verify(exchange.getResponse(), times(0)).setStatusCode(HttpStatus.UNAUTHORIZED);
    }

    @Test
    void testSuccessfulAuthorization() {

        Jwt jwt = createJwt("test-client", Arrays.asList("admin", "user"));
        when(jwtDecoder.decode(anyString())).thenReturn(Mono.just(jwt));
        when(chain.filter(exchange)).thenReturn(Mono.empty());
        GatewayFilter gatewayFilter = filter.apply(Map.of("test-client", "admin,user"));
        StepVerifier.create(gatewayFilter.filter(exchange, chain)).verifyComplete();

        verify(exchange.getResponse(), times(0)).setStatusCode(HttpStatus.UNAUTHORIZED);
    }

    private Jwt createJwt(String clientId, List<String> roles) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("client_id", clientId);
        claims.put(ROLE_CLAIM, roles);

        return new Jwt("token", Instant.now(), Instant.now().plusSeconds(3600), Map.of("alg", "RS256"), claims);
    }

    private ServerWebExchange mockExchangeWithAuthHeader() {
        ServerHttpRequest request = mock(ServerHttpRequest.class);
        ServerHttpResponse response = mock(ServerHttpResponse.class);
        ServerWebExchange exchange = mock(ServerWebExchange.class);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer someToken");

        when(request.getHeaders()).thenReturn(headers);
        when(exchange.getRequest()).thenReturn(request);
        when(exchange.getResponse()).thenReturn(response);

        return exchange;
    }
}
