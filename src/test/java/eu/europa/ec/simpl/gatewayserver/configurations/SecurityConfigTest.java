package eu.europa.ec.simpl.gatewayserver.configurations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

@ExtendWith(MockitoExtension.class)
class SecurityConfigTest {
    @Mock
    private RouteConfig routeConfig;

    @Mock
    private SwaggerConfig swaggerConfig;

    @Mock
    private CorsProperties corsProperties;

    private ServerHttpSecurity serverHttpSecurity;

    @InjectMocks
    private SecurityConfig securityConfig;

    @BeforeEach
    void setup() {
        serverHttpSecurity = ServerHttpSecurity.http();
    }

    @Test
    void testKeycloakProxy() {
        SecurityWebFilterChain securityWebFilterChain = securityConfig.keycloakProxy(serverHttpSecurity);
        assertNotNull(securityWebFilterChain);

        var filters = securityWebFilterChain.getWebFilters().collectList().block();
        assertNotNull(filters);

        var hasHeadersFilter = filters.stream()
                .anyMatch(filter -> filter.getClass().getSimpleName().contains("HeadersWebFilter"));
        var hasCorsFilter = filters.stream()
                .anyMatch(filter -> filter.getClass().getSimpleName().contains("CorsWebFilter"));
        var hasCsrfFilter = filters.stream()
                .anyMatch(filter -> filter.getClass().getSimpleName().contains("CsrfWebFilter"));

        assertThat(hasHeadersFilter).isFalse();
        assertThat(hasCorsFilter).isFalse();
        assertThat(hasCsrfFilter).isFalse();
    }

    @Test
    void testCorsConfigurationSource() throws IllegalAccessException {
        when(corsProperties.allowedOrigins()).thenReturn(List.of("http://localhost:3000", "https://example.com"));
        when(corsProperties.allowedHeaders()).thenReturn(List.of("Authorization", "Content-Type"));

        var corsConfigurationSource = securityConfig.corsConfigurationSource(corsProperties);

        assertNotNull(corsConfigurationSource);
        assertInstanceOf(UrlBasedCorsConfigurationSource.class, corsConfigurationSource);

        var urlBasedSource = (UrlBasedCorsConfigurationSource) corsConfigurationSource;

        var corsConfigurationsField =
                ReflectionUtils.findField(UrlBasedCorsConfigurationSource.class, "corsConfigurations");
        assert corsConfigurationsField != null;
        ReflectionUtils.makeAccessible(corsConfigurationsField);
        @SuppressWarnings("unchecked")
        var corsConfigurations = (Map<Object, CorsConfiguration>) corsConfigurationsField.get(urlBasedSource);
        assertNotNull(corsConfigurations);

        var key = corsConfigurations.keySet().iterator().next();
        var corsConfiguration = corsConfigurations.get(key);

        assertNotNull(corsConfiguration);
        assertEquals(corsProperties.allowedOrigins(), corsConfiguration.getAllowedOrigins());
        assertEquals(corsProperties.allowedHeaders(), corsConfiguration.getAllowedHeaders());
        assertTrue(Objects.requireNonNull(corsConfiguration.getAllowedMethods()).contains("*"));
    }
}
