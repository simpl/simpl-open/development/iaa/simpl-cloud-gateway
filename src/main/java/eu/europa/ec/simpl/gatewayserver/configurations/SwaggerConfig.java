package eu.europa.ec.simpl.gatewayserver.configurations;

import java.util.Set;
import org.springdoc.core.properties.AbstractSwaggerUiConfigProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "springdoc.swagger-ui")
public record SwaggerConfig(Set<AbstractSwaggerUiConfigProperties.SwaggerUrl> urls) {}
