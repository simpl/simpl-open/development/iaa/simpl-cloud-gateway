package eu.europa.ec.simpl.gatewayserver.configurations;

import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(value = "cors")
public record CorsProperties(List<String> allowedOrigins, List<String> allowedHeaders) {}
