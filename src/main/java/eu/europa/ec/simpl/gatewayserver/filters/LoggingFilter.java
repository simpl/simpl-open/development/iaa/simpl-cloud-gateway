package eu.europa.ec.simpl.gatewayserver.filters;

import static eu.simpl.MessageBuilder.buildMessage;

import eu.europa.ec.simpl.common.utils.JwtUtil;
import eu.europa.ec.simpl.gatewayserver.configurations.LoggingRule;
import eu.europa.ec.simpl.gatewayserver.configurations.RouteConfig;
import eu.simpl.types.LogMessage;
import eu.simpl.types.MessageType;
import java.util.*;
import java.util.function.Function;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.lookup.StrSubstitutor;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Log4j2
@Component
public class LoggingFilter implements WebFilter {

    private final List<LoggingRule> loggingRules;

    public LoggingFilter(RouteConfig routeConfig) {
        this.loggingRules = Optional.ofNullable(routeConfig)
                .map(RouteConfig::logging)
                .map(RouteConfig.Logging::business)
                .orElse(List.of());
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        return Flux.fromIterable(loggingRules)
                .filterWhen(rule -> rule.matches(exchange))
                .next()
                .map(buildLogger(exchange, chain))
                .flatMap(BusinessLogger::log)
                .switchIfEmpty(chain.filter(exchange));
    }

    private Function<LoggingRule, BusinessLogger> buildLogger(ServerWebExchange exchange, WebFilterChain chain) {
        return loggingRule -> new BusinessLogger(loggingRule.config(), exchange, chain);
    }

    private class BusinessLogger {

        private final LoggingRule.Config config;
        private final ServerWebExchange exchange;
        private final WebFilterChain chain;
        private final UUID correlationId;

        private BusinessLogger(LoggingRule.Config config, ServerWebExchange exchange, WebFilterChain chain) {
            this.config = config;
            this.exchange = exchange;
            this.chain = chain;
            this.correlationId = UUID.randomUUID();
        }

        public Mono<Void> log() {
            return log(MessageType.REQUEST)
                    .then(chain.filter(exchange).doAfterTerminate(() -> log(MessageType.RESPONSE)));
        }

        private Mono<Void> log(MessageType messageType) {
            var token = JwtUtil.getBearerToken(exchange.getRequest().getHeaders());

            var user = token.map(getUser()).orElse("anonymous user");

            Map<String, String> templateContext = getTemplateContext(messageType, user);

            var messageTemplate = config.getMessage();

            log.log(
                    Level.getLevel("BUSINESS"),
                    buildMessage(LogMessage.builder()
                            .businessOperations(config.getOperations())
                            .messageType(messageType)
                            .origin(user)
                            .destination(user)
                            .correlationId(correlationId.toString())
                            .msg(StrSubstitutor.replace(messageTemplate, templateContext))
                            .build()));

            return Mono.empty();
        }

        private Function<String, String> getUser() {
            return token -> JwtUtil.getClaim(JwtUtil.parseJwt(token), "email").toString();
        }

        private Map<String, String> getTemplateContext(MessageType messageType, String user) {
            return Map.of(
                    "method", exchange.getRequest().getMethod().toString(),
                    "path", exchange.getRequest().getPath().value(),
                    "user", user,
                    "responseStatus", getResponseStatus(messageType));
        }

        private String getResponseStatus(MessageType messageType) {
            if (messageType == MessageType.REQUEST) {
                return "";
            } else {
                return Optional.of(exchange.getResponse())
                        .map(ServerHttpResponse::getStatusCode)
                        .map(Objects::toString)
                        .orElse(StringUtils.EMPTY);
            }
        }
    }
}
