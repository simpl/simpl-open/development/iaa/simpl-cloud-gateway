package eu.europa.ec.simpl.gatewayserver.configurations;

import eu.europa.ec.simpl.gatewayserver.matchers.CliMatcher;
import eu.europa.ec.simpl.gatewayserver.matchers.KeycloakMatcher;
import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;
import lombok.extern.log4j.Log4j2;
import org.springdoc.core.properties.AbstractSwaggerUiConfigProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatcher;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatchers;
import org.springframework.util.CollectionUtils;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

@Log4j2
@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {

    private final RouteConfig routeConfig;
    private final List<ServerWebExchangeMatcher> publicMatchers;
    private final List<ServerWebExchangeMatcher> deniedMatchers;
    private final SwaggerConfig swaggerConfig;

    public SecurityConfig(RouteConfig routeConfig, SwaggerConfig swaggerConfig) {
        this.routeConfig = routeConfig;
        this.swaggerConfig = swaggerConfig;
        this.publicMatchers = buildPublicUrls();
        this.deniedMatchers = buildMatchers(routeConfig.deniedUrls());
    }

    @Bean
    @Order(1)
    public SecurityWebFilterChain keycloakProxy(ServerHttpSecurity security) {
        return security.securityMatcher(new KeycloakMatcher())
                .headers(headerSpec -> headerSpec.frameOptions(ServerHttpSecurity.HeaderSpec.FrameOptionsSpec::disable))
                .csrf(ServerHttpSecurity.CsrfSpec::disable)
                .cors(ServerHttpSecurity.CorsSpec::disable)
                .build();
    }

    @Bean
    @Order(5)
    public SecurityWebFilterChain cliAuth(ServerHttpSecurity security) {
        return security.securityMatcher(new CliMatcher())
                .authorizeExchange(exchange -> exchange.anyExchange().authenticated())
                .oauth2ResourceServer(oauth2 -> oauth2.jwt(Customizer.withDefaults()))
                .csrf(ServerHttpSecurity.CsrfSpec::disable)
                .build();
    }

    @Bean
    @Order(10)
    public SecurityWebFilterChain jwtAuth(ServerHttpSecurity security) {
        return security.authorizeExchange(request -> {
                    if (!CollectionUtils.isEmpty(routeConfig.publicUrls())) {
                        log.info("configuring public urls {}", routeConfig.publicUrls());
                        request.matchers(publicMatchers.toArray(ServerWebExchangeMatcher[]::new))
                                .permitAll();
                    }

                    if (!CollectionUtils.isEmpty(routeConfig.deniedUrls())) {
                        log.info("configuring urls to deny {}", routeConfig.deniedUrls());
                        request.matchers(deniedMatchers.toArray(ServerWebExchangeMatcher[]::new))
                                .denyAll();
                    }

                    routeConfig.rbac().forEach(rule -> configureRbacRule(request, rule));

                    request.pathMatchers("**").authenticated().anyExchange().denyAll();
                })
                .oauth2ResourceServer(oauth2 -> oauth2.jwt(Customizer.withDefaults()))
                .csrf(ServerHttpSecurity.CsrfSpec::disable)
                .build();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource(CorsProperties corsProperties) {
        var configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(corsProperties.allowedOrigins());
        configuration.setAllowedHeaders(corsProperties.allowedHeaders());
        configuration.setAllowedMethods(List.of("*"));
        var source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    private void configureRbacRule(ServerHttpSecurity.AuthorizeExchangeSpec request, Rule rule) {
        request.pathMatchers(rule.method(), rule.path()).hasAnyRole(rule.roles().toArray(String[]::new));
    }

    private List<ServerWebExchangeMatcher> buildPublicUrls() {
        var out = new ArrayList<ServerWebExchangeMatcher>();
        addSwaggersPublicUrls(out);
        out.addAll(buildMatchers(routeConfig.publicUrls()));
        return out;
    }

    private void addSwaggersPublicUrls(List<ServerWebExchangeMatcher> publicUrl) {
        UnaryOperator<String> extractBasePath = v3Url -> v3Url.replaceAll("^([^\"]*/)(?=v3)", "$1");

        swaggerConfig.urls().stream()
                .map(AbstractSwaggerUiConfigProperties.SwaggerUrl::getUrl)
                .map(v3url -> ServerWebExchangeMatchers.pathMatchers(
                        HttpMethod.GET,
                        "%s/swagger-ui.html".formatted(extractBasePath.apply(v3url)),
                        "%s/swagger-ui/**".formatted(extractBasePath.apply(v3url)),
                        "%s/**".formatted(v3url)))
                .forEach(publicUrl::add);
    }

    private List<ServerWebExchangeMatcher> buildMatchers(List<Rule> rules) {
        if (rules == null) {
            return new ArrayList<>();
        }
        return rules.stream()
                .map(rule -> ServerWebExchangeMatchers.pathMatchers(rule.method(), rule.path()))
                .toList();
    }
}
