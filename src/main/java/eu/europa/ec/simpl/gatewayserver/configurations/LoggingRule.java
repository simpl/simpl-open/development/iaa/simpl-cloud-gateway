package eu.europa.ec.simpl.gatewayserver.configurations;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import lombok.Data;
import org.springframework.http.HttpMethod;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatcher;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatchers;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

public record LoggingRule(
        HttpMethod method, String path, List<MultiMapRule> query, List<MultiMapRule> header, Config config) {

    public record MultiMapRule(String name, String value, List<String> values) {

        public Boolean matches(MultiValueMap<String, String> queryParams) {
            return queryParams.containsKey(name)
                    && (values().isEmpty()
                            || values().stream()
                                    .anyMatch(value -> queryParams.get(name).contains(value)));
        }

        @Override
        public List<String> values() {
            return Optional.ofNullable(values)
                    .orElse(Optional.ofNullable(value).map(List::of).orElse(List.of()));
        }
    }

    @Data
    public static class Config {
        private String message = "${method} ${path} ${responseStatus}";
        private List<String> operations;
    }

    public Mono<Boolean> matches(ServerWebExchange exchange) {

        return ServerWebExchangeMatchers.pathMatchers(method(), path)
                .matches(exchange)
                .map(ServerWebExchangeMatcher.MatchResult::isMatch)
                .map(isMatch -> isMatch && matchQuery(exchange) && matchHeaders(exchange));
    }

    private boolean matchQuery(ServerWebExchange exchange) {
        return Optional.ofNullable(query())
                .map(Collection::stream)
                .map(query -> query.map(q -> q.matches(exchange.getRequest().getQueryParams()))
                        .reduce(Boolean::logicalAnd)
                        .orElse(true))
                .orElse(true);
    }

    private boolean matchHeaders(ServerWebExchange exchange) {
        return Optional.ofNullable(header())
                .map(Collection::stream)
                .map(header -> header.map(h -> h.matches(exchange.getRequest().getHeaders()))
                        .reduce(Boolean::logicalAnd)
                        .orElse(true))
                .orElse(true);
    }
}
